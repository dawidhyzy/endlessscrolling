package com.github.dawidhyzy.endlessscrolling

import android.app.Application

import timber.log.Timber
import kotlin.properties.Delegates

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
class App : Application() {

    var appComponent: AppComponent by Delegates.notNull<AppComponent>()
        private set

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        initAppComponent()
    }

    private fun initAppComponent() {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}