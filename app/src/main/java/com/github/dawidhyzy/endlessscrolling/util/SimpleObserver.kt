package com.github.dawidhyzy.endlessscrolling.util

import rx.Observer

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
open class SimpleObserver<T> : Observer<T> {
    override fun onCompleted() {

    }


    override fun onError(e: Throwable) {

    }


    override fun onNext(t: T) {

    }
}
