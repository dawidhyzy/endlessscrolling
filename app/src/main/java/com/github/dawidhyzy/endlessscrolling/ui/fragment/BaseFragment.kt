package com.github.dawidhyzy.endlessscrolling.ui.fragment

import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.github.dawidhyzy.endlessscrolling.App
import com.github.dawidhyzy.endlessscrolling.AppComponent
import com.github.dawidhyzy.endlessscrolling.R
import com.github.dawidhyzy.endlessscrolling.data.api.PropertyFinderApi

import javax.inject.Inject

import org.jetbrains.anko.find
import kotlin.properties.Delegates

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
abstract class BaseFragment : Fragment() {

    protected var rootView: View by Delegates.notNull<View>()

    val mToolbar: Toolbar? by lazy { rootView.find<Toolbar>(R.id.toolbar) }

    lateinit @Inject internal var mPropertyFinderApi: PropertyFinderApi

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        this.applicationComponent.inject(this)
        rootView = inflater!!.inflate(layoutResource, container, false)

        mToolbar?.let{
            (activity as AppCompatActivity).setSupportActionBar(it)
        }

        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        init(savedInstanceState)
        super.onViewCreated(view, savedInstanceState)
    }

    protected val applicationComponent: AppComponent
        get() = (activity.application as App).appComponent

    protected abstract val layoutResource: Int

    protected abstract fun init(savedInstanceState: Bundle?)
}
