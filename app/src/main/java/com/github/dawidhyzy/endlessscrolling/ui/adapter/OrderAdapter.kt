package com.github.dawidhyzy.endlessscrolling.ui.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import com.github.dawidhyzy.endlessscrolling.data.Order
import org.jetbrains.anko.find

/**
 * @author Dawid Hyży
 * *
 * @since 26/11/15.
 */
class OrderAdapter(context: Context, resource: Int, objects: List<Order>) : ArrayAdapter<Order>(context, resource, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view = super.getView(position, convertView, parent)
        view?.let { it.find<TextView>(android.R.id.text1).text = getItem(position).name }
        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view = super.getDropDownView(position, convertView, parent)
        view?.let { it.find<TextView>(android.R.id.text1).text = getItem(position).name }
        return view
    }

}
