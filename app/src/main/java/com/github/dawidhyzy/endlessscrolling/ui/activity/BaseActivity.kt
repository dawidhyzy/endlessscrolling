package com.github.dawidhyzy.endlessscrolling.ui.activity

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

import com.github.dawidhyzy.endlessscrolling.R

import org.jetbrains.anko.find

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
open class BaseActivity : AppCompatActivity() {

    val mToolbar: Toolbar? by lazy { find<Toolbar>(R.id.toolbar) }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(layoutResID)
        mToolbar?.let { setSupportActionBar(mToolbar) }
    }


}
