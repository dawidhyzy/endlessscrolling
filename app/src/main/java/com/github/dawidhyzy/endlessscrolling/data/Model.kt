package com.github.dawidhyzy.endlessscrolling.data

import com.google.gson.annotations.SerializedName

/**
 * @author Dawid Hyży
 * @since 24/02/16.
 */

data class Result(val total : Int, @SerializedName("res") val properties : MutableList<Property>)

data class Property(val id: Int, @SerializedName("category_id") val categoryId: Int,
                    val title: String, val subject: String, val type: String,
                    val thumbnail: String, @SerializedName("thumbnail_big") val thumbnailBig: String,
                    @SerializedName("image_count") val imageCount: Int, val price: String,
                    @SerializedName("price_period") val pricePeriod: String,
                    @SerializedName("price_label") val priceLabel: String,
                    @SerializedName("price_value") val priceValue: String, val currency: String,
                    val featured: String, val location: String, val area: String, val poa: String,
                    val bathrooms: Int, val bedrooms: Int, val visited: Boolean,
                    @SerializedName("amenities") val amenities: MutableList<String>,
                    @SerializedName("lat") val latitude: Double,
                    @SerializedName("long") val longitude: Double) {
}

data class Order(val name: String, val value: String)