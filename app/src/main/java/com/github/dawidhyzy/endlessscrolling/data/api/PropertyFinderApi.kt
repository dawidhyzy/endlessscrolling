package com.github.dawidhyzy.endlessscrolling.data.api

import com.github.dawidhyzy.endlessscrolling.data.Result

import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
interface PropertyFinderApi {

    @GET("/mobileapi")
    fun getProperties(@Query("page") pageNumber: Int?, @Query("order") order: String): Observable<Result>
}
