package com.github.dawidhyzy.endlessscrolling.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.github.dawidhyzy.endlessscrolling.R
import com.github.dawidhyzy.endlessscrolling.data.Property
import com.github.dawidhyzy.endlessscrolling.extensions.inflate

import java.util.ArrayList
import com.github.dawidhyzy.endlessscrolling.extensions.loadUrl
import org.jetbrains.anko.find

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
class PropertyAdapter : RecyclerView.Adapter<PropertyAdapter.ViewHolder>() {

    private var properties: MutableList<Property> = ArrayList()

    fun setProperties(properties: MutableList<Property>) {
        this.properties = properties
        notifyDataSetChanged()
    }

    fun addProperties(properties: MutableList<Property>) {
        var previousCount = this.properties.size
        this.properties.addAll(properties)
        notifyItemRangeInserted(previousCount, properties.size)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.row_property))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(properties[position]){
            holder.title.text = title
            holder.bedrooms.text = "Bedrooms: $bedrooms"
            holder.price.text = "Price $price"
            holder.thumbnail.loadUrl(thumbnail);
        }
    }

    override fun getItemCount(): Int = properties.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val thumbnail: ImageView = itemView.find(R.id.thumbnail)
        val title: TextView = itemView.find(R.id.name)
        val bedrooms: TextView = itemView.find(R.id.bedrooms)
        val price: TextView = itemView.find(R.id.price)
    }
}
