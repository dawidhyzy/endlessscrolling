package com.github.dawidhyzy.endlessscrolling.ui.activity

import android.os.Bundle

import com.github.dawidhyzy.endlessscrolling.R

class ListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
    }
}
