package com.github.dawidhyzy.endlessscrolling.ui.fragment

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Spinner
import com.github.dawidhyzy.endlessscrolling.R
import com.github.dawidhyzy.endlessscrolling.data.Order
import com.github.dawidhyzy.endlessscrolling.data.Property
import com.github.dawidhyzy.endlessscrolling.extensions.d
import com.github.dawidhyzy.endlessscrolling.list.ListPresenter
import com.github.dawidhyzy.endlessscrolling.list.View
import com.github.dawidhyzy.endlessscrolling.ui.adapter.EndlessRecyclerOnScrollListener
import com.github.dawidhyzy.endlessscrolling.ui.adapter.OrderAdapter
import com.github.dawidhyzy.endlessscrolling.ui.adapter.PropertyAdapter
import com.jakewharton.rxbinding.widget.AdapterViewItemSelectionEvent
import com.jakewharton.rxbinding.widget.AdapterViewNothingSelectionEvent
import com.jakewharton.rxbinding.widget.selectionEvents
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter
import jp.wasabeef.recyclerview.animators.FadeInAnimator
import org.jetbrains.anko.support.v4.find

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
class ListFragment() : BaseFragment(), View, EndlessRecyclerOnScrollListener.OnLoadMoreListener {

    override val layoutResource: Int
        get() = R.layout.fragment_list;

    private val mPropertyAdapter = PropertyAdapter()

    private val linearLayoutManager = LinearLayoutManager(activity)
    private val mEndlessRecyclerOnScrollListener = EndlessRecyclerOnScrollListener(linearLayoutManager, this)
    private val mListPresenter by lazy { ListPresenter(this, mPropertyFinderApi) }
    private val scaleInAnimationAdapter = ScaleInAnimationAdapter(AlphaInAnimationAdapter(mPropertyAdapter));

    private val refresh by lazy { find<SwipeRefreshLayout>(R.id.refresh) }
    private val recycler by lazy { find<RecyclerView>(R.id.recycler) }
    private val spinner by lazy { find<Spinner>(R.id.spinner) }


    override fun init(savedInstanceState: Bundle?) {
        refresh.isEnabled = false
        recycler.setHasFixedSize(true)
        recycler.layoutManager = linearLayoutManager
        recycler.addOnScrollListener(mEndlessRecyclerOnScrollListener)
        recycler.itemAnimator = FadeInAnimator()
        recycler.adapter = scaleInAnimationAdapter
        spinner.selectionEvents()
                .filter { it !is AdapterViewNothingSelectionEvent }
                .map { it as AdapterViewItemSelectionEvent }
                .subscribe { onOrderSelected(it.position()) }
        mListPresenter.loadOrderList()
    }

    override fun showLoading(show: Boolean) {
        refresh.isRefreshing = show
    }

    override fun setProperties(properties: MutableList<Property>) {
        mPropertyAdapter.setProperties(properties)
    }

    override fun loadedMore(properties: MutableList<Property>) {
        mPropertyAdapter.addProperties(properties)
    }

    override fun setOrderList(orderList: List<Order>) {
        spinner.adapter = OrderAdapter(activity,
                android.R.layout.simple_dropdown_item_1line,
                orderList)
    }

    override fun onLoadMore(currentPage: Int) {
        d { "Load more page: $currentPage" }
        mListPresenter.loadMoreProperties(currentPage, spinner.selectedItem as Order)

    }

    internal fun onOrderSelected(position: Int) {
        mEndlessRecyclerOnScrollListener.clear()
        val order = spinner.adapter.getItem(position) as Order
        mListPresenter.loadProperties(order)
    }

}
