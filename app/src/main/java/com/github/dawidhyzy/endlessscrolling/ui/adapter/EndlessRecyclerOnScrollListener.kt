package com.github.dawidhyzy.endlessscrolling.ui.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.github.dawidhyzy.endlessscrolling.extensions.d

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
class EndlessRecyclerOnScrollListener(private val mLinearLayoutManager: LinearLayoutManager, private val mOnLoadMoreListener: EndlessRecyclerOnScrollListener.OnLoadMoreListener?) : RecyclerView.OnScrollListener() {


    private var previousTotal = 0 // The total number of items in the data-set after the last load
    private var loading = true // True if we are still waiting for the last set of data to load.
    private val visibleThreshold = 5 // The minimum amount of items to have below your current scroll position before loading more.
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount: Int = 0
    internal var totalItemCount: Int = 0


    private var currentPage = 1


    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        d { "Scrolled before: previous total: $previousTotal, loading: $loading, first visible item: $firstVisibleItem,visible item count: $visibleItemCount, total item count: $totalItemCount" }

        visibleItemCount = recyclerView!!.childCount
        totalItemCount = mLinearLayoutManager.itemCount
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount != previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            // End has been reached


            // Do something
            currentPage++

            mOnLoadMoreListener?.onLoadMore(currentPage)

            loading = true
        }
        d { "Scrolled after: previous total: $previousTotal, loading: $loading, first visible item: $firstVisibleItem,visible item count: $visibleItemCount, total item count: $totalItemCount" }
    }

    fun clear() {
        previousTotal = 0
    }

    interface OnLoadMoreListener {
        fun onLoadMore(currentPage: Int)
    }

}
