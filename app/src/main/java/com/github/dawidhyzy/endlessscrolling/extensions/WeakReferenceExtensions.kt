package com.github.dawidhyzy.endlessscrolling.extensions

import java.lang.ref.WeakReference

/**
 * @author Dawid Hyży
 * @since 25/02/16.
 */

inline fun <T> WeakReference<T>.safe(body : T.() -> Unit) {
    this.get()?.body()
}