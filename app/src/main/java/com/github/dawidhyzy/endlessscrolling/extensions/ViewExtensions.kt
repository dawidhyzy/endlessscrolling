package com.github.dawidhyzy.endlessscrolling.extensions

import android.support.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.github.dawidhyzy.endlessscrolling.R
import com.squareup.picasso.Picasso

/**
 * @author Dawid Hyży
 * @since 24/02/16.
 */

fun ImageView.loadUrl(url: String?, placeholder : Int = R.drawable.placeholder) {
    Picasso.with(context).load(url).placeholder(placeholder).fit().centerCrop().into(this)
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false) : View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}