package com.github.dawidhyzy.endlessscrolling.list

import com.github.dawidhyzy.endlessscrolling.data.Property
import com.github.dawidhyzy.endlessscrolling.data.Order

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */

interface View {
    fun showLoading(show: Boolean)
    fun setProperties(properties: MutableList<Property>)
    fun loadedMore(properties: MutableList<Property>)
    fun setOrderList(orderList: List<Order>)
}

interface Presenter {
    fun loadProperties(order: Order)
    fun loadOrderList()
    fun loadMoreProperties(page: Int, order: Order)
    fun onOrderSelected(order: Order)
}
