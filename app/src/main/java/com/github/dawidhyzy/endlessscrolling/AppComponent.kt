package com.github.dawidhyzy.endlessscrolling

import com.github.dawidhyzy.endlessscrolling.ui.fragment.BaseFragment

import javax.inject.Singleton

import dagger.Component

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(baseFragment: BaseFragment)
}
