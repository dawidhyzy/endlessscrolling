package com.github.dawidhyzy.endlessscrolling.list

import com.github.dawidhyzy.endlessscrolling.data.Order
import com.github.dawidhyzy.endlessscrolling.data.Property
import com.github.dawidhyzy.endlessscrolling.data.api.PropertyFinderApi
import com.github.dawidhyzy.endlessscrolling.extensions.safe
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.lang.ref.WeakReference
import java.util.*

/**
 * @author Dawid Hyży
 * *
 * @since 25/11/15.
 */
class ListPresenter(listView: View, private val propertyFinderApi: PropertyFinderApi) : Presenter {

    private val view: WeakReference<View> = WeakReference(listView)

    private val orderList = Arrays.asList(Order("Price descending", "pd"),
            Order("Price ascending", "pa"),
            Order("Bedrooms descending", "bd"),
            Order("Bedrooms ascending", "ba"))

    override fun loadProperties(order: Order) {
        propertyFinderApi.getProperties(0, order.value)
                .map { it.properties}
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.safe { showLoading(true) }
                }
                .subscribe(
                        { properties : MutableList<Property> ->
                            view.safe {
                                showLoading(false)
                                setProperties(properties)
                            }
                        },
                        { throwable : Throwable ->
                            view.safe { showLoading(false) }
                            throwable.printStackTrace()
                        }

                )

    }

    override fun loadOrderList() {
        view.safe { setOrderList(orderList) }
    }

    override fun loadMoreProperties(page: Int, order: Order) {
        propertyFinderApi.getProperties(page, order.value)
                .map{ it.properties }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.safe { showLoading(true) }
                }
                .subscribe(
                        { properties : MutableList<Property> ->
                            view.safe {
                                showLoading(false)
                                loadedMore(properties)
                            }
                        },
                        { throwable : Throwable ->
                            view.safe { showLoading(false) }
                            throwable.printStackTrace()
                        }
                )
    }

    override fun onOrderSelected(order: Order) {
        propertyFinderApi.getProperties(0, order.value)
                .map{ it.properties }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe {
                    view.safe { showLoading(true) }
                }
        .subscribe(
                { properties : MutableList<Property> ->
                    view.safe {
                        showLoading(false)
                        setProperties(properties)
                    }
                },
                { throwable : Throwable ->
                    view.safe { showLoading(false) }
                    throwable.printStackTrace()

                }
        )
    }

}
